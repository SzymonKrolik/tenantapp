import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RegisterComponent } from './user/register/register.component';
import {AccountActivationComponent} from "./user/register/account-activation/account-activation.component";
import {RegisterSuccessComponent} from "./user/register/register-success/register-success.component";
import {RouterModule} from "@angular/router";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {LoginComponent} from "./user/login/login.component";
import { AccountListComponent } from './user/account-list/account-list.component';
import { UserPanelComponent } from './user/user-panel/user-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    AccountActivationComponent,
    RegisterSuccessComponent,
    AccountListComponent,
    UserPanelComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {path: 'register', component: RegisterComponent},
      {path: 'register-success', component: RegisterSuccessComponent},
      {path: 'registration/confirm/:token', component: AccountActivationComponent},
      {path: 'login', component: LoginComponent},
      {path: 'account-list', component:AccountListComponent},
      {path: 'panel', component: UserPanelComponent},
      {path: '', redirectTo: '/register', pathMatch: 'full'},
    ]),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

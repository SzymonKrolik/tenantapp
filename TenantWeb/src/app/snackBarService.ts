import {Component, Injectable, OnInit} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";




@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(private snackBar: MatSnackBar ) {
  }

  public showSnackBar(message: string, err: boolean) {
    console.log('show snakc')
    this.snackBar.open(message, 'Close', {
      duration: 60000,
      panelClass: [err ? 'red-snackbar' : 'blue-snackbar']
    })
  }
}

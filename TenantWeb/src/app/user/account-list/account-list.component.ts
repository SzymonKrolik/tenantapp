import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Services} from "../services";
import {SnackbarService} from "../../snackBarService";
import {CookieService} from "ngx-cookie-service";
import {catchError, throwError} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {AccountDTO} from "../interface/AccountDTO";

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {

  private userData: any;
  public accounts: AccountDTO[];
  constructor(private router: Router, private userService: Services, private snackbarService: SnackbarService, private cookieService: CookieService) {
  }
  ngOnInit() {
    // @ts-ignore
    this.getUserAccountList();
  }

  private getUserAccountList(): void {
    this.userService.getUserAccounts().pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this.snackbarService.showSnackBar(err.error.message, true);
      }
      return throwError(err);
    })).subscribe((res: any) => {
      this.accounts = res;
      console.log('ACCOUNT: ' + this.accounts);
    })
  }

  public loginToAccount(uuid: any) {
    const loginRequest = {
      uuid: uuid
    }
    this.userService.loginToAccount(loginRequest).pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 409) {
        this.snackbarService.showSnackBar('Konto nie zostało aktywowane lub jest zablokowane, proszę o kontakt z administratorem.', true);
      } else if (err.status === 408) {
        this.snackbarService.showSnackBar(err.error.message, true);
      }
      return throwError(err);
    })).subscribe((res: any) => {
      sessionStorage.setItem('TOKEN', res.token);
      sessionStorage.setItem('currentUser', JSON.stringify(res));
      this.router.navigate(['/panel'])
    });
  }
}

export interface AccountDTO {
  name: string;
  accountType: string;
  email: string;
  uuid: string;
}

export interface UserDataDTO {
  name: string;
  email: string;
  phoneNumber: string;
  birthDate: string;
  city: string;
}

import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Services} from "../services";
import {SnackbarService} from "../../snackBarService";
import {catchError, throwError} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public form: FormGroup;

  constructor(private router: Router, private userService: Services, private snackbarService: SnackbarService, private cookieService: CookieService) {
  }
  ngOnInit(): void {
    this.buildForm();
  }
  private buildForm() {
    this.form = new FormGroup({
      email : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      password : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
    })
  }

  public login(): void {
    if (this.form.invalid) {
      return;
    }
    console.log(this.form.getRawValue());
    this.userService.loginUser(this.form.getRawValue()).pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 409) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 400) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 404) {
        this.snackbarService.showSnackBar('Wprowadzono nieprawidłowy login lub hasło', true);
      }
      return throwError(err);
    })).subscribe((res: any) => {
      this.router.navigate(['account-list']);
      sessionStorage.setItem('TOKEN', res.token);
      sessionStorage.setItem('currentUser', JSON.stringify(res));
    })
  }

}

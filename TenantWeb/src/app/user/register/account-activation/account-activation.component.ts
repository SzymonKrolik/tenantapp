import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Services} from "../../services";
import {SnackbarService} from "../../../snackBarService";
import {catchError, throwError} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-account-activation',
  templateUrl: './account-activation.component.html',
  styleUrls: ['./account-activation.component.css']
})
export class AccountActivationComponent implements OnInit {
  public form: FormGroup;
  private userInformation: any;
  public isCompany = false;
  private token: string | null;
  public validLink: boolean;
  constructor(private router: Router, private userService: Services, private snackbarService: SnackbarService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this.validateToken();

  }

  private buildForm() {
    this.form = new FormGroup({
      confirmationToken : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
    })
  }

  private validateToken() {
    this.userService.validateToken(this.token).pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 409) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 400) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 408) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      }
      return throwError(err);
    })).subscribe(res => {

    })
  }

  public activateAccount() {

  }

  private generateConfirmationToken() {

  }


}

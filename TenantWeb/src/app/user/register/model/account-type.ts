export interface AccountType {
  value: string;
  name: string;
}

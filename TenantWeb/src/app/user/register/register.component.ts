import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {Services} from "../services";
import {catchError, throwError} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {SnackbarService} from "../../snackBarService";
import {AccountType} from "./model/account-type";
import {CreateUserDTO} from "../interface/CreateUserDTO";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
  public form: FormGroup;
  public accountTypes: AccountType[];
  public isCompany = false;
  private createUserDto: CreateUserDTO;

  constructor(private router: Router, private userService: Services, private snackbarService: SnackbarService) {
  }

  ngOnInit(): void {
    this.buildForm();
  }
  private buildForm() {
    this.form = new FormGroup({
      email : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      phoneNumber : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      name : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      lastName : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      password : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      matchingPassword : new FormControl({value : '', disabled: false}, [Validators.required, Validators.maxLength(50)]),
      birthDate: new FormControl({value:'', disabled: false}, [Validators.required]),
    })
  }

  public register() {
    if (this.form.invalid) {
      console.log('INVLAID');
      this.getFormValidationErrors();
      return;
    }
    console.log(this.form.getRawValue());
    this.createUserDto = {...this.createUserDto,...this.form.value}
    this.createUserDto.password = btoa(this.createUserDto.password);
    this.createUserDto.matchingPassword = btoa(this.createUserDto.matchingPassword);

    this.userService.userRegister(this.createUserDto).pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 500) {
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 409) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      } else if (err.status === 400) {
        console.log(err.message);
        this.snackbarService.showSnackBar(err.error.message, true);
      }
      return throwError(err);
    })).subscribe(res => {
      this.router.navigate(['register-success']);
    })
  }

  getFormValidationErrors() {
    Object.keys(this.form.controls).forEach(key => {
      // @ts-ignore
      const controlErrors: ValidationErrors = this.form.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }
}

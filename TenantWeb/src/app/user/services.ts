import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environments} from "../../environments/environments";
import {CookieService} from "ngx-cookie-service";
import {CreateUserDTO} from "./interface/CreateUserDTO";

// @ts-ignore
const httpOptions = {
  headers: new HttpHeaders({
    'Content-type' : 'application/json',
    'Access-Control-Allow-Origin' : '*',
    'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
  })
}

@Injectable({
  providedIn: 'root'
})
export class Services {
  constructor(protected http: HttpClient, private cookieService: CookieService) {
  }

  public userRegister(dto: CreateUserDTO) {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
    });
    return this.http.post(`${environments.userApiUrl}register`, dto);
  }

  public verifyToken(token: string | null) {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
    });
    return this.http.post(`${environments.userApiUrl}/confirm`, token, {headers: reqHeader});
  }

  public loginUser(request: any): any {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
    });
    return this.http.post<any>(`${environments.userApiUrl}login`, request);
  }


  validateToken(token: any) {

    return this.http.post<any>(`${environments.userApiUrl}register/validate-token`, token);
  }

  getUserAccounts() {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
    });
    return this.http.get<any>(`${environments.userApiUrl}user/accounts`,  { headers: reqHeader });
  }

  loginToAccount(uuid: any) {
    var reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + sessionStorage.getItem('TOKEN') || '{}'
    });
    return this.http.post<any>(`${environments.userApiUrl}login/login-to-account`, uuid, {headers: reqHeader});
  }
}

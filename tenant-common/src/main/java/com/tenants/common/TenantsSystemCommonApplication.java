package com.tenants.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TenantsSystemCommonApplication {
  public static void main(String[] args) {
    SpringApplication.run(TenantsSystemCommonApplication.class, args);
  }

}

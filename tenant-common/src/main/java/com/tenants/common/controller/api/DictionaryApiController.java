package com.tenants.common.controller.api;

import com.tenants.common.dto.DictionaryDTO;
import com.tenants.common.service.DictionaryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/dictionary")
@AllArgsConstructor
public class DictionaryApiController {

    private final DictionaryService dictionaryService;

    @GetMapping("/by-code")
    @ResponseBody
    public DictionaryDTO getParameterByCode(@RequestParam("code") String code) {
        return dictionaryService.getDictionaryByCode(code);
    }
}
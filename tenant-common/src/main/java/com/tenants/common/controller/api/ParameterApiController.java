package com.tenants.common.controller.api;


import com.tenants.common.dto.ParameterDTOResponse;
import com.tenants.common.service.ParameterService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/parameter")
@AllArgsConstructor
public class ParameterApiController {
    private final ParameterService parameterService;

    @GetMapping("/by-code")
    @ResponseBody
    public ParameterDTOResponse getParameterByCode(@RequestParam("code") String code) {
        return parameterService.getParameterByCode(code);
    }
}
package com.tenants.common.controller.form;

import com.tenants.common.dto.ParameterDTOResponse;
import com.tenants.common.service.ParameterService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/parameter")
@AllArgsConstructor
@Slf4j
public class ParameterController {
    private final ParameterService parameterService;

    @GetMapping("/by-code")
    @ResponseBody
    public ParameterDTOResponse getParameterByCode(@RequestParam("code") String code) {
        return parameterService.getParameterByCode(code);
    }
}

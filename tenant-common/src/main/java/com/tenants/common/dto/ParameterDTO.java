package com.tenants.common.dto;

import com.tenants.common.entity.Parameter;
import lombok.Data;

@Data
public class ParameterDTO {
    private String code;
    private String value;
    public static ParameterDTO of (Parameter parameter) {
        ParameterDTO dto = new ParameterDTO();

        dto.setCode(parameter.getCode());
        dto.setValue(parameter.getValue());

        return dto;
    }

}

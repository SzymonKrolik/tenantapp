package com.tenants.common.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParameterDTOResponse {
    private String code;
    private String value;
}

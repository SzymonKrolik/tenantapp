package com.tenants.common.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import lombok.*;

import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DICTIONARY")
public class Dictionary {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DICTIONARY_ID")
    private Long id;

    @Column(name = "DICTIONARY_CODE")
    private String code;

    @Column(name = "DICTIONARY_NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String descripiton;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "dictionary")
    private Set<DictionaryValues> dictionaryValuesSet;
}

package com.tenants.common.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

import lombok.*;


@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "DICTIONARY_VALUES")
public class DictionaryValues {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DICTIONARY_VALUE_ID")
    private Long id;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DICTIONARY_ID")
    private Dictionary dictionary;

    @Column(name = "VALUE")
    private String value;

}

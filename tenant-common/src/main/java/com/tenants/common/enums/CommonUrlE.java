package com.tenants.common.enums;

import lombok.Getter;

public enum CommonUrlE {
    GET_PARAMETER_BY_VALUE("/api/parameter/by-code"),
    GET_DICTIONARY_BY_VALUE("/api/dictionary/by-code");

    @Getter
    private String url;

    CommonUrlE(String url) {
        this.url = url;
    }
}

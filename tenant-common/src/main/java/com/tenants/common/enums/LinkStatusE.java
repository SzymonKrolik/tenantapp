package com.tenants.common.enums;

public enum LinkStatusE {
    ACTIVE,
    EXPIRED,
    USED

}

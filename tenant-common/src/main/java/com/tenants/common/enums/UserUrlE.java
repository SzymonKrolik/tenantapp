package com.tenants.common.enums;

import lombok.Getter;

public enum UserUrlE {
    GET_USER_BY_TOKEN_URL("internal/user/by-token");


    @Getter
    private String url;

    UserUrlE(String url) {
        this.url = url;
    }
}

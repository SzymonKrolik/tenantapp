package com.tenants.common.exception;

public class DictionaryNotFoundException extends RuntimeException {
    private static final String MSG = "Dictionary not found";

    public DictionaryNotFoundException() {
        super(MSG);
    }
}

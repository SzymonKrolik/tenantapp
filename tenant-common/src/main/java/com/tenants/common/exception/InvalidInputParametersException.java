package com.tenants.common.exception;

public class InvalidInputParametersException extends RuntimeException {
    private static final String MSG = "Wprowadzaone dane są niepoprawne";

    public InvalidInputParametersException() {
        super(MSG);
    }
}

package com.tenants.common.repository;

import com.tenants.common.entity.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DictionaryRepostitory extends JpaRepository<Dictionary, Long> {
    Optional<Dictionary> findByCode(String code);
}

package com.tenants.common.repository;

import com.tenants.common.entity.DictionaryValues;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DictionaryValueRepository extends JpaRepository<DictionaryValues, Long> {
}

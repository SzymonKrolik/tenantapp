package com.tenants.common.service;

import com.tenants.common.dto.DictionaryDTO;
import com.tenants.common.entity.Dictionary;
import com.tenants.common.entity.DictionaryValues;
import com.tenants.common.exception.DictionaryNotFoundException;
import com.tenants.common.repository.DictionaryRepostitory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class DictionaryService {
    private final DictionaryRepostitory dictionaryRepostitory;

    public DictionaryDTO getDictionaryByCode(String code) {
        Dictionary dictionary = dictionaryRepostitory.findByCode(code).orElseThrow(DictionaryNotFoundException::new);
        DictionaryDTO response = new DictionaryDTO();
        response.setCode(dictionary.getCode());
        response.setValues(dictionary.getDictionaryValuesSet().stream()
                .map(DictionaryValues::getValue)
                .collect(Collectors.toSet()));

        return response;
    }
}
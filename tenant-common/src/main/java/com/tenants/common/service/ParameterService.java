package com.tenants.common.service;

import com.tenants.common.dto.ParameterDTO;
import com.tenants.common.dto.ParameterDTOResponse;
import com.tenants.common.entity.Parameter;
import com.tenants.common.exception.ParameterNotFoundException;
import com.tenants.common.repository.ParameterRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ParameterService {
    private final ParameterRepository parameterRepository;
    //todo mapper i jeszcze controller advisor
    public ParameterDTOResponse getParameterByCode(String code) {
        List<Parameter> all = parameterRepository.findAll();
        ParameterDTO parameterDTO =  ParameterDTO.of(parameterRepository.findByCode(code).orElseThrow(ParameterNotFoundException::new));
        return ParameterDTOResponse.builder()
                .code(parameterDTO.getCode())
                .value(parameterDTO.getValue())
                .build();
    }
}

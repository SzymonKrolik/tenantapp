package com.tenants.common.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CookieUtils {
    private static final String TOKEN_COOKIE = "TOKEN";
    public static void killAllCookies(HttpServletRequest request, HttpServletResponse response) {
        if (request.getCookies() != null) {
            for (Cookie cookie : request.getCookies()) {
                response.addCookie(invalidateCookie(cookie));
            }
        }
    }

    private static Cookie invalidateCookie(Cookie cookie) {
        cookie.setMaxAge(0);
        cookie.setHttpOnly(Boolean.TRUE);
        cookie.setSecure(Boolean.TRUE);

        return cookie;
    }

    public static Cookie createCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setHttpOnly(Boolean.FALSE);
        return cookie;
    }

    public static String getCookieValue(String cookieName, HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals(cookieName))
                return cookies[i].getValue();
        }

        throw new RuntimeException("API ERROR");
    }

    public static String getUserToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals(TOKEN_COOKIE))
                return cookies[i].getValue();
        }

        throw new RuntimeException("API ERROR");
    }


}

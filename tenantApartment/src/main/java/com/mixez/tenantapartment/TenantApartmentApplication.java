package com.mixez.tenantapartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TenantApartmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TenantApartmentApplication.class, args);
    }

}

package com.mixez.tenantapartment.client;



import com.mixez.tenantapartment.dto.DictionaryDTO;
import com.mixez.tenantapartment.dto.ParameterDTOResponse;
import com.mixez.tenantapartment.enums.CommonUrlE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@Slf4j
public class CommonClient {
    private final WebClient commonWebClient;

    public CommonClient(@Value("${common.url}") String commonUrl) {
        this.commonWebClient = WebClient.builder()
                .baseUrl(commonUrl)
                .build();
    }
    //todo ogarnac wyjatki jaki rzuca ta usluga
    public ParameterDTOResponse getParameterByCode(String code) {
        return commonWebClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(CommonUrlE.GET_PARAMETER_BY_VALUE.getUrl())
                        .queryParam("code", code)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ParameterDTOResponse.class)
                .block();
    }

    public DictionaryDTO getDictionaryByCode(String code) {
        return commonWebClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(CommonUrlE.GET_DICTIONARY_BY_VALUE.getUrl())
                        .queryParam("code", code)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(DictionaryDTO.class)
                .block();
    }
}

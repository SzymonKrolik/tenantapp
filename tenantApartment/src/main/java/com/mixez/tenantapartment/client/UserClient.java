package com.mixez.tenantapartment.client;

import com.mixez.tenantapartment.dto.UserInformationDTO;
import com.mixez.tenantapartment.dto.UserUuidRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@Slf4j
public class UserClient {
    private final WebClient userClient;

    public UserClient(@Value("${user.url}") String userUrl) {
        this.userClient = WebClient.builder()
                .baseUrl(userUrl)
                .build();
    }

    public UserInformationDTO getUserInformationByUuid(String uuid) {
        return userClient.post()
                .uri("/api/user")
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(UserUuidRequest.builder().uuid(uuid).build()))
                .retrieve()
                .bodyToMono(UserInformationDTO.class)
                .block();
    }
}

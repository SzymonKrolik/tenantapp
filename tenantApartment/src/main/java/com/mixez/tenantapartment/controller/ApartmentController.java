package com.mixez.tenantapartment.controller;


import com.mixez.tenantapartment.dto.*;
import com.mixez.tenantapartment.service.ApartmentService;
import com.mixez.tenantapartment.utils.PageUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/apartment")
@AllArgsConstructor
public class ApartmentController {

    private final ApartmentService apartmentService;
    @PostMapping
    public void createAparment(@RequestBody CreateAparmentRequest request, HttpServletRequest httpServletRequest) {
        apartmentService.createApartment(httpServletRequest, request);
    }

    @PostMapping("/get-all")
    public Page<BasicApartmentInformationDTO> getAllUserApartment(@RequestBody SearchApartmentRequest dto, HttpServletRequest request) {
            return apartmentService.getAllByPagination(dto, request, PageUtils.createPagination(dto));
    }

    @GetMapping("{id}")
    public ApartmentInformationDTO getApartmentFullInformation(@PathVariable("id") Long id, HttpServletRequest request) {
        return apartmentService.getApartmentFullInformation(id, request);
    }

    @PutMapping("/edit")
    public void updateApartmentInformation(@RequestBody UpdateApartmentRequest dto, HttpServletRequest request) {
        apartmentService.updateApartment(dto);
    }
}

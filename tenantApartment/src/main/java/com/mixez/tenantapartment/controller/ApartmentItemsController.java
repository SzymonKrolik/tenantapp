package com.mixez.tenantapartment.controller;


import com.mixez.tenantapartment.dto.AddItemsToApartmentRequest;
import com.mixez.tenantapartment.dto.ApartmentItemDTO;
import com.mixez.tenantapartment.dto.ApartmentItemSearchRequest;
import com.mixez.tenantapartment.dto.RemoveItemFromApartmentRequest;
import com.mixez.tenantapartment.service.ApartmentItemsService;
import com.mixez.tenantapartment.utils.PageUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/apartment-item")
@AllArgsConstructor
public class ApartmentItemsController {
    private final ApartmentItemsService apartmentItemsService;

    @PostMapping
    public void addItemsToApartment(@RequestBody AddItemsToApartmentRequest dto, HttpServletRequest request) {
        apartmentItemsService.addItemsToApartment(dto);
    }

    @DeleteMapping
    public void deleteItemFromApartment(@RequestBody RemoveItemFromApartmentRequest dto, HttpServletRequest request) {
        apartmentItemsService.removeItemFromApartment(dto);
    }

    @GetMapping("/by-apartment")
    public Page<ApartmentItemDTO> getAllItems(@RequestBody ApartmentItemSearchRequest dto, HttpServletRequest request) {
        return apartmentItemsService.getAllByPagination(dto, PageUtils.createPagination(dto));
    }
}

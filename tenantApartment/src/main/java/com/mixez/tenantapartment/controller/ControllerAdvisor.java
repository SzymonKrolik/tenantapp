package com.mixez.tenantapartment.controller;



import com.mixez.tenantapartment.dto.ExceptionDetailsDTO;
import com.mixez.tenantapartment.exception.ApartmentAlreadyExistException;
import com.mixez.tenantapartment.exception.InvalidRequestBodyException;
import com.mixez.tenantapartment.exception.MaxApartmentsLimitException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
@RequiredArgsConstructor
public class ControllerAdvisor {

    @ExceptionHandler({ApartmentAlreadyExistException.class})
    public ResponseEntity<?> handleConflictException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({InvalidRequestBodyException.class})
    public ResponseEntity<?> handleBadRequestException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({MaxApartmentsLimitException.class})
    public ResponseEntity<?> handlePaymentException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.PAYMENT_REQUIRED);
    }


}


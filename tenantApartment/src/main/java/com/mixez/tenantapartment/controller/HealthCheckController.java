package com.mixez.tenantapartment.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.GET;

@RestController
@RequestMapping("/healthcheck")
@AllArgsConstructor
public class HealthCheckController {

    @GetMapping
    public String healthCheck() {
        return "Apartment works";
    }
}

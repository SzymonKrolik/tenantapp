package com.mixez.tenantapartment.dto;

import lombok.Data;

import java.util.List;

@Data
public class AddItemsToApartmentRequest {
    private List<ApartmentItemDTO> apartmentItemDTO;
    private Long apartmentId;
}

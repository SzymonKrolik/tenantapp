package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class ApartmentAddressRequest {

    private String city;
    private String street;
    private String number;
    private String apartmentNumber;
}

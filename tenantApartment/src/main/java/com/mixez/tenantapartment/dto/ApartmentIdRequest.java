package com.mixez.tenantapartment.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class ApartmentIdRequest {


    private Long apartmentId;
}

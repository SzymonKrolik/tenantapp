package com.mixez.tenantapartment.dto;

import com.mixez.tenantapartment.enums.ApartmentTypeE;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ApartmentInformationDTO {
    private String name;
    private String floor;

    private int rooms;
    private Double livingArea;
    private Double rent;
    private Boolean media;
    private Boolean available;
    private ApartmentAddressRequest address;
    private String bankAccountNumber;
    private String bankName;
    private String moneyTransferTitle;
    private int invoiceSendDate;
    private int paymentDate;
    private Set<ApartmentItemDTO> apartmentItemDTOList;
    private ApartmentTypeE apartmentType;
    private String roomNumber;

}

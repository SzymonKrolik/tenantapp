package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class ApartmentItemDTO {
    private String itemName;
    private String quantity;
    private String status;
}

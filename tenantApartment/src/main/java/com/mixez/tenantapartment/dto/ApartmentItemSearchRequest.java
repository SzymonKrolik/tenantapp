package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class ApartmentItemSearchRequest extends DefaultPaginationRequest {
    private String status;
    private Long apartmentId;
    private String ownerUuid;
}

package com.mixez.tenantapartment.dto;


import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.entity.ApartmentItems;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
public class ApartmentItemSpecification implements Specification<ApartmentItems> {
    private final ApartmentItemSearchRequest request;
    @Override
    public Predicate toPredicate(Root<ApartmentItems> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();
        query.distinct(Boolean.TRUE);

        if (Objects.nonNull(request.getApartmentId())) {
            Join<Apartment, ApartmentItems> apartmentItemsJoin = root.join("apartment");
            predicateList.add(criteriaBuilder.equal(apartmentItemsJoin.get("id"), request.getApartmentId()));
        }

        if (Objects.nonNull(request.getStatus())) {
            predicateList.add(criteriaBuilder.equal(root.get("status"), request.getStatus()));
        }

        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));


    }
}

package com.mixez.tenantapartment.dto;

import com.mixez.tenantapartment.entity.Address;
import com.mixez.tenantapartment.entity.Apartment;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
public class ApartmentSpecification implements Specification<Apartment> {

    private final SearchApartmentRequest request;
    @Override
    public Predicate toPredicate(Root<Apartment> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicateList = new ArrayList<>();
        query.distinct(Boolean.TRUE);

        if (Objects.nonNull(request.getStreet())) {
            Join<Address, Apartment> apartmentJoin = root.join("address");
            predicateList.add(criteriaBuilder.like(apartmentJoin.get("street"), "%" + request.getStreet() + "%"));
        }

        if (Objects.nonNull(request.getCity())) {
            Join<Address, Apartment> apartmentJoin = root.join("address");
            predicateList.add(criteriaBuilder.like(apartmentJoin.get("city"), "%" + request.getCity() + "%"));
        }

        if (Objects.nonNull(request.getOwnerUuid())) {
            predicateList.add(criteriaBuilder.equal(root.get("ownerUuid"), request.getOwnerUuid()));
        }

        if (Objects.nonNull(request.getAvailable())) {
            predicateList.add(criteriaBuilder.equal(root.get("available"), request.getAvailable().equals(Boolean.TRUE) ? "1" : "2"));
        }

        return criteriaBuilder.and(predicateList.toArray(new Predicate[predicateList.size()]));

    }
}

package com.mixez.tenantapartment.dto;

import com.mixez.tenantapartment.enums.ApartmentTypeE;
import lombok.Data;

@Data
public class BasicApartmentInformationDTO {
    private String name;
    private String city;
    private String street;
    private Boolean available;
    private Long id;
    private ApartmentTypeE apartmentType;

}

package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class CreateAparmentRequest {
    private String name;
    private String floor;

    private int rooms;
    private Double livingArea;
    private Double rent;
    private Boolean media;
    private Boolean available;
    private ApartmentAddressRequest address;
    private String bankAccountNumber;
    private String bankName;
    private String moneyTransferTitle;
    private int invoiceSendDate;
    private int paymentDate;
    private Boolean equipment;
    private Double paymentMedia;
    private String apartmentType;
    private String roomNumber;
}

package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class DefaultPaginationRequest {
    int pageNo;
    int pageSize;
    String sortBy;
    String sortDir;

}

package com.mixez.tenantapartment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@Data
public class DictionaryDTO {
   @JsonProperty("code")
   private String code;
   @JsonProperty("values")
   private Set<String> values;
}

package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class RemoveItemFromApartmentRequest {
    private Long apartmentId;
    private Long itemId;
}

package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class SearchApartmentRequest extends DefaultPaginationRequest {
    private String city;
    private String street;
    private String ownerUuid;

    private Boolean available;
}

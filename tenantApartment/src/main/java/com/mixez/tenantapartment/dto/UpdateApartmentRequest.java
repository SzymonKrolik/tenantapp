package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class UpdateApartmentRequest {
    private String name;
    private String floor;
    private int rooms;
    private Double livingArea;
    private Double rent;
    private Boolean media;
    private Boolean available;
    private Long apartmentId;
    private String bankAccountNumber;
    private String bankName;
    private String moneyTransferTitle;
    private int paymentDate;
    private int invoiceSendDate;
    private Boolean equipment;
    private Double paymentMedia;
    private String apartmentType;

}

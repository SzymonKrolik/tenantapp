package com.mixez.tenantapartment.dto;

import lombok.Data;

@Data
public class UserInformationDTO {
    private String accountType;
    private String uuid;
    private String email;

}

package com.mixez.tenantapartment.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserUuidRequest {
    private String uuid;
}

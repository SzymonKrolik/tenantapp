package com.mixez.tenantapartment.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ADDRESS")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ADDRESS_ID")
    private Long id;

    @Column(nullable = false, name = "CITY")
    private String city;

    @Column(nullable = false, name = "STREET")
    private String street;

    @Column(nullable = false, name = "NUMBER")
    private String number;

    @Column(nullable = true, name = "APARTMENT_NUMBER")
    private String apartmentNumber;

    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "address")
    private Apartment apartment;
}

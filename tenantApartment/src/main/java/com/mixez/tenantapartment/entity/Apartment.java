package com.mixez.tenantapartment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mixez.tenantapartment.enums.ApartmentTypeE;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "APARTMENT")
public class Apartment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APARTMENT_ID")
    private Long id;

    @Column(name = "APARTMENT_NAME")
    private String name;

    @Column(name = "FLOOR")
    private String floor;

    @Column(name = "ROOMS")
    private int rooms;

    @Column(name = "LIVING_AREA")
    private Double livingArea;

    @Column(name = "RENT")
    private BigDecimal rent;

    @Column(name = "MEDIA")
    private Boolean media;

    @Column(name = "AVAILABLE")
    private Boolean available;

    @Column(name = "OWNER_UUID")
    private String ownerUuid;

    @Column(name = "BANK_ACCOUNT_NUMBER")
    private String bankAccountNumber;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "MONEY_TRANSFER_TITLE")
    private String moneyTransferTitle;

    @Column(name = "INVOICE_SEND_DATE")
    private int invoiceSendDate;

    @Column(name = "INVOICE_PAYMENT_DATE")
    private int paymentDate;

    @Column(name = "EQUIPMENT")
    private Boolean equipment;

    @Column(name = "PAYMENT_MEDIA")
    private BigDecimal paymentMedia;

    @Column(name = "APARTMENT_TYPE")
    private ApartmentTypeE apartmentType;

    @Column(name = "ROOM_NUMBER")
    private String roomNumber;

    @EqualsAndHashCode.Exclude
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "APARTMENT_ADDRESS_ID", referencedColumnName = "ADDRESS_ID")
    private Address address;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "apartment")
    private Set<Invoice> apartmentInvoices;

    @JsonIgnore
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "apartment")
    private Set<ApartmentItems> apartmentItems;
}

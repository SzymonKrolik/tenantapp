package com.mixez.tenantapartment.enums;

import lombok.Getter;

public enum ApartmentTypeE {
    APARTMENT("APARTMENT"),
    ROOM("ROOM"),
    HOUSE("HOUSE"),
    GARAGE("GARAGE");

    @Getter
    private String type;

    ApartmentTypeE(String type) {
        this.type = type;
    }

    public static ApartmentTypeE getApartmentType(String type) {
        for (ApartmentTypeE typeE : values()) {
            if (typeE.getType().equalsIgnoreCase(type))
                return typeE;
        }

        return null;
    }
}

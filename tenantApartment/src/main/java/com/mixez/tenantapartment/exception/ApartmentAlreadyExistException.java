package com.mixez.tenantapartment.exception;

public class ApartmentAlreadyExistException extends RuntimeException {
    private static final String MSG = "Aparment already exist";

    public ApartmentAlreadyExistException() {
        super(MSG);
    }
}

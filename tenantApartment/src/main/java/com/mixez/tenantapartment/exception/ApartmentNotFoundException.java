package com.mixez.tenantapartment.exception;

public class ApartmentNotFoundException extends RuntimeException {
    private static final String MSG = "Apartment not found";

    public ApartmentNotFoundException() {
        super(MSG);
    }
}

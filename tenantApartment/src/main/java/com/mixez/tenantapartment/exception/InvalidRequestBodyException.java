package com.mixez.tenantapartment.exception;

public class InvalidRequestBodyException extends RuntimeException {
    private static final String MSG = "Invalid request body";

    public InvalidRequestBodyException() {
        super(MSG);
    }
}

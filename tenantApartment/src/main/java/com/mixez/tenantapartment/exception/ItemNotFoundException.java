package com.mixez.tenantapartment.exception;

public class ItemNotFoundException extends RuntimeException {
    private static final String MSG = "Item not found";

    public ItemNotFoundException() {
        super(MSG);
    }
}

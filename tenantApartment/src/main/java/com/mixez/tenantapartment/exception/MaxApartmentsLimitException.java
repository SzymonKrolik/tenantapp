package com.mixez.tenantapartment.exception;

public class MaxApartmentsLimitException extends RuntimeException {
    private static final String MSG = "Max apartments limit";

    public MaxApartmentsLimitException() {
        super(MSG);
    }
}

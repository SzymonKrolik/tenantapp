package com.mixez.tenantapartment.mapper;


import com.mixez.tenantapartment.dto.ApartmentAddressRequest;
import com.mixez.tenantapartment.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ApartmentAddressMapper {
    Address toEntity(ApartmentAddressRequest request);

}

package com.mixez.tenantapartment.mapper;


import com.mixez.tenantapartment.dto.ApartmentItemDTO;
import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.entity.ApartmentItems;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ApartmentItemMapper {

    ApartmentItems toEntity(ApartmentItemDTO dto);
    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "itemName", source = "itemName")
    ApartmentItemDTO toDTO(ApartmentItems items);
}

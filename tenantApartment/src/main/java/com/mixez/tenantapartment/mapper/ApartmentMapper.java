package com.mixez.tenantapartment.mapper;


import com.mixez.tenantapartment.dto.ApartmentInformationDTO;
import com.mixez.tenantapartment.dto.BasicApartmentInformationDTO;
import com.mixez.tenantapartment.dto.CreateAparmentRequest;
import com.mixez.tenantapartment.dto.UpdateApartmentRequest;
import com.mixez.tenantapartment.entity.Apartment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ApartmentMapper {


    @Mapping(target = "available", source = "available")
    @Mapping(target = "media", source = "media")
    @Mapping(target = "bankName", source = "bankName")
    @Mapping(target = "moneyTransferTitle", source = "moneyTransferTitle")
    @Mapping(target = "bankAccountNumber", source = "bankAccountNumber")
    @Mapping(target = "invoiceSendDate", source = "invoiceSendDate")
    @Mapping(target = "paymentDate", source = "paymentDate")
    @Mapping(target = "paymentMedia", source = "paymentMedia")
    @Mapping(target = "equipment", source = "equipment")
    Apartment toEntity(CreateAparmentRequest request);

    @Mapping(target = "city", source = "address.city")
    @Mapping(target = "street", source = "address.street")
    @Mapping(target = "id", source = "id")
    @Mapping(target = "apartmentType", source = "apartmentType")
    BasicApartmentInformationDTO toBasicInformation(Apartment apartment);

    @Mapping(target = "address.city", source = "address.city")
    @Mapping(target = "address.street", source = "address.street")
    @Mapping(target = "address.number", source = "address.number")
    @Mapping(target = "address.apartmentNumber", source = "address.apartmentNumber")
    @Mapping(target = "roomNumber", source = "roomNumber")
    ApartmentInformationDTO toInformation(Apartment apartment);

    @Mapping(target = "available", source = "available")
    @Mapping(target = "media", source = "media")
    Apartment toEntity(UpdateApartmentRequest request);


}

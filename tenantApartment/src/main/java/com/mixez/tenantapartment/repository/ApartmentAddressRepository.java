package com.mixez.tenantapartment.repository;

import com.mixez.tenantapartment.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApartmentAddressRepository extends JpaRepository<Address, Long> {
    Boolean existsByStreetAndCityAndNumberAndApartmentNumber(String street, String city, String number, String apartmentNumber);
}

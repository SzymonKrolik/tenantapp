package com.mixez.tenantapartment.repository;

import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.entity.ApartmentItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ApartmentItemsRepository extends JpaRepository<ApartmentItems, Long>, JpaSpecificationExecutor<ApartmentItems> {
    Optional<ApartmentItems> findByIdAndApartment(Long id, Apartment apartment);

    Set<ApartmentItems> findByApartment(Apartment apartment);
}

package com.mixez.tenantapartment.repository;

import com.mixez.tenantapartment.entity.Apartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApartmentRepository extends JpaRepository<Apartment, Long>, JpaSpecificationExecutor<Apartment> {
    int countAllByOwnerUuid(String ownerUuid);

    Optional<Apartment> findByIdAndOwnerUuid(Long id, String ownerUuid);


}

package com.mixez.tenantapartment.service;


import com.mixez.tenantapartment.dto.*;
import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.entity.ApartmentItems;
import com.mixez.tenantapartment.exception.ItemNotFoundException;
import com.mixez.tenantapartment.mapper.ApartmentItemMapper;
import com.mixez.tenantapartment.repository.ApartmentItemsRepository;
import com.mixez.tenantapartment.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ApartmentItemsService {
    private final ApartmentItemsRepository apartmentItemsRepository;
    private final ApartmentService apartmentService;
    private final ApartmentItemMapper apartmentItemMapper;

    public ApartmentItemsService(ApartmentItemsRepository apartmentItemsRepository, @Lazy ApartmentService apartmentService, ApartmentItemMapper apartmentItemMapper) {
        this.apartmentItemsRepository = apartmentItemsRepository;
        this.apartmentService = apartmentService;
        this.apartmentItemMapper = apartmentItemMapper;
    }

    public void addItemsToApartment(AddItemsToApartmentRequest dto) {
        Apartment apartment = apartmentService.getApartmentByIdAndOwnerUuid(dto.getApartmentId(), Utils.getUserUuid());

        Set<ApartmentItems> apartmentItemsList = dto.getApartmentItemDTO().stream()
                .map(apartmentItemMapper::toEntity)
                .collect(Collectors.toSet());
        apartmentItemsList.forEach(apartmentItems -> apartmentItems.setApartment(apartment));
        apartmentItemsRepository.saveAll(apartmentItemsList);
        log.info("APARTMENT_ITEMS_SAVED_APARTMENT_ID: {}", apartment.getId());
    }

    public void removeItemFromApartment(RemoveItemFromApartmentRequest dto) {
        Apartment apartment = apartmentService.getApartmentByIdAndOwnerUuid(dto.getApartmentId(), Utils.getUserUuid());
        ApartmentItems apartmentItems = getApartmentItemByIdAndApartment(dto.getItemId(), apartment);

        apartmentItemsRepository.delete(apartmentItems);
        apartment.getApartmentItems().remove(apartmentItems);
        apartmentService.saveApartment(apartment);
    }

    public ApartmentItems getApartmentItemByIdAndApartment(Long itemId, Apartment apartment) {
        return apartmentItemsRepository.findByIdAndApartment(itemId, apartment).orElseThrow(ItemNotFoundException::new);
    }

    public Page<ApartmentItemDTO> getAllByPagination(ApartmentItemSearchRequest dto, Pageable pagination) {

        dto.setOwnerUuid(Utils.getUserUuid());
        Page<ApartmentItems> apartmentItemsPage = apartmentItemsRepository.findAll(new ApartmentItemSpecification(dto), pagination);
        List<ApartmentItems> apartmentItemsList = apartmentItemsPage.getContent()
                .stream().filter(item -> item.getApartment().getOwnerUuid().equalsIgnoreCase(Utils.getUserUuid()))
                .toList();

        List<ApartmentItemDTO> response = new ArrayList<>(apartmentItemsList.stream()
                .map(apartmentItemMapper::toDTO)
                .toList());

        return new PageImpl<>(response, pagination, response.size());
    }

    public Set<ApartmentItemDTO> getItemsByApartment(Apartment apartment) {
        return apartmentItemsRepository.findByApartment(apartment).stream()
                .map(apartmentItemMapper::toDTO)
                .collect(Collectors.toSet());
    }
}

package com.mixez.tenantapartment.service;

import com.mixez.tenantapartment.client.CommonClient;
import com.mixez.tenantapartment.client.UserClient;
import com.mixez.tenantapartment.dto.*;
import com.mixez.tenantapartment.entity.Address;
import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.enums.ApartmentTypeE;
import com.mixez.tenantapartment.exception.ApartmentAlreadyExistException;
import com.mixez.tenantapartment.exception.ApartmentNotFoundException;
import com.mixez.tenantapartment.exception.InvalidRequestBodyException;
import com.mixez.tenantapartment.exception.MaxApartmentsLimitException;
import com.mixez.tenantapartment.mapper.ApartmentAddressMapper;
import com.mixez.tenantapartment.mapper.ApartmentMapper;
import com.mixez.tenantapartment.repository.ApartmentAddressRepository;
import com.mixez.tenantapartment.repository.ApartmentRepository;
import com.mixez.tenantapartment.utils.Utils;

import jdk.jshell.execution.Util;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class ApartmentService {
    private final ApartmentRepository apartmentRepository;
    private final ApartmentAddressRepository apartmentAddressRepository;
    private final UserClient userClient;
    private final CommonClient commonClient;
    private final ApartmentMapper apartmentMapper;
    private final ApartmentAddressMapper apartmentAddressMapper;
    private final ApartmentItemsService apartmentItemsService;



    public void createApartment(HttpServletRequest request, CreateAparmentRequest dto) {
        validateCreateApartmentRequest(dto);
        String userUid = Utils.getUserUuid();
        validateApartmentsLimit(userUid);
        validateApartmentAddress(dto.getAddress());
        UserInformationDTO userInformationDTO = userClient.getUserInformationByUuid(userUid);
        Apartment apartment = apartmentMapper.toEntity(dto);
        apartment.setOwnerUuid(userInformationDTO.getUuid());
        ApartmentTypeE apartmentTypeE = ApartmentTypeE.getApartmentType(dto.getApartmentType());
        if (Objects.isNull(apartmentTypeE))
            throw new InvalidRequestBodyException();
        apartment.setApartmentType(apartmentTypeE);
        Address address = apartmentAddressMapper.toEntity(dto.getAddress());

        apartment.setAddress(address);

        try {
            apartmentRepository.save(apartment);
            apartmentAddressRepository.save(address);
            log.info("Aparment save: {} user: {}", apartment.getName(), userUid);
        } catch (Exception ex) {
            log.info("Apartment save error: {} user: {}", ex.getMessage(), userUid);
        }
    }

    private void validateApartmentsLimit(String userUid) {
        UserInformationDTO actualUser = userClient.getUserInformationByUuid(userUid);
        ParameterDTOResponse maxApartmentParameter = commonClient.getParameterByCode("MAX_APARTMENT_USER");
        int userApartments = apartmentRepository.countAllByOwnerUuid(actualUser.getUuid());

        if (userApartments >= Integer.parseInt(maxApartmentParameter.getValue()))
            throw new MaxApartmentsLimitException();
    }

    //todo ustalkic dokladna walidacje, numer konta itp tez
    private void validateCreateApartmentRequest(CreateAparmentRequest dto) {
        if (Objects.isNull(dto)) {
            throw new InvalidRequestBodyException();
        }
    }

    private void validateApartmentAddress(ApartmentAddressRequest addressRequest) {
        if (apartmentAddressRepository.existsByStreetAndCityAndNumberAndApartmentNumber(addressRequest.getStreet(), addressRequest.getCity(), addressRequest.getNumber(), addressRequest.getApartmentNumber()))
            throw new ApartmentAlreadyExistException();
    }

    public Page<BasicApartmentInformationDTO> getAllByPagination(SearchApartmentRequest dto, HttpServletRequest request, Pageable pagination) {
        String uuid = Utils.getUserUuid();
        dto.setOwnerUuid(uuid);
        return apartmentRepository.findAll(new ApartmentSpecification(dto), pagination).map(apartmentMapper::toBasicInformation);

    }

    public ApartmentInformationDTO getApartmentFullInformation(Long id, HttpServletRequest request) {
        Apartment apartment = getApartmentByIdAndOwnerUuid(id, Utils.getUserUuid());
        Set<ApartmentItemDTO> apartmentItems = apartmentItemsService.getItemsByApartment(apartment);

        ApartmentInformationDTO response = apartmentMapper.toInformation(apartment);
        response.setApartmentItemDTOList(apartmentItems);

        return response;
    }

    public Apartment getApartmentByIdAndOwnerUuid(Long id, String ownerUuid) {
        return apartmentRepository.findByIdAndOwnerUuid(id, ownerUuid).orElseThrow(ApartmentNotFoundException::new);
    }

    public void updateApartment(UpdateApartmentRequest dto) {
        Apartment apartment = getApartmentByIdAndOwnerUuid(dto.getApartmentId(), Utils.getUserUuid());
        validateUpadateRequest(dto);
        apartment.setFloor(dto.getFloor());
        apartment.setAvailable(dto.getAvailable());
        apartment.setName(dto.getName());
        apartment.setMedia(dto.getMedia());
        apartment.setRent(BigDecimal.valueOf(dto.getRent()));
        apartment.setRooms(dto.getRooms());
        apartment.setLivingArea(dto.getLivingArea());
        apartment.setBankName(dto.getBankName());
        apartment.setBankAccountNumber(dto.getBankAccountNumber());
        apartment.setMoneyTransferTitle(dto.getMoneyTransferTitle());
        ApartmentTypeE apartmentTypeE = ApartmentTypeE.getApartmentType(dto.getApartmentType());
        if (Objects.isNull(apartmentTypeE))
            throw new InvalidRequestBodyException();
        apartment.setApartmentType(apartmentTypeE);
        apartmentRepository.save(apartment);
    }

    private void validateUpadateRequest(UpdateApartmentRequest dto) {
        if (Objects.isNull(dto.getRent()) || Objects.isNull(dto.getMedia()) || Objects.isNull(dto.getFloor()) || Objects.isNull(dto.getAvailable()) || Objects.isNull(dto.getLivingArea()) || Objects.isNull(dto.getName()))
            throw new InvalidRequestBodyException();
    }

    public void saveApartment(Apartment apartment) {
        apartmentRepository.save(apartment);
    }
}

package com.mixez.tenantapartment.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mixez.tenantapartment.dto.ApartmentIdRequest;
import com.mixez.tenantapartment.entity.Apartment;
import com.mixez.tenantapartment.utils.Utils;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.tool.schema.SchemaToolingLogging.LOGGER;

@Service
@AllArgsConstructor
public class InvoiceService {
    private final ApartmentService apartmentService;

    public void createInvoice(ApartmentIdRequest dto) {
        Apartment apartment = apartmentService.getApartmentByIdAndOwnerUuid(dto.getApartmentId(), Utils.getUserUuid());
        generateInvoiceFor("test");

    }

    // Generating PDF invoice
    public void generateInvoiceFor(String pdfInvoiceParameters) {
        InputStream pdfJasperReportXml;
        try {
            pdfJasperReportXml = getClass().getClassLoader().getResourceAsStream("invoice_template.jrxml");

            // Load invoice jrxml template.getBytes
            JasperDesign design = JRXmlLoader.load(new ByteArrayInputStream(pdfJasperReportXml.readAllBytes()));
            JasperReport report = JasperCompileManager.compileReport(design);

            // Create parameters map.
            Map<String, Object> parameters = buildParametrsMap(pdfInvoiceParameters);

            // Fill the template with parameters map.
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, new JREmptyDataSource());

            JRPdfExporter exporter = new JRPdfExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(
                    new SimpleOutputStreamExporterOutput("invoiceReport.pdf"));

            SimplePdfReportConfiguration reportConfig
                    = new SimplePdfReportConfiguration();
            reportConfig.setSizePageToContent(true);
            reportConfig.setForceLineBreakPolicy(false);

            SimplePdfExporterConfiguration exportConfig
                    = new SimplePdfExporterConfiguration();
            exportConfig.setMetadataAuthor("lantem");
            exportConfig.setEncrypted(true);
            exportConfig.setAllowedPermissionsHint("PRINTING");

            exporter.setConfiguration(reportConfig);
            exporter.setConfiguration(exportConfig);

            exporter.exportReport();
        } catch (JsonProcessingException jsonEx) {
            LOGGER.error("Error has occured {}", jsonEx);
        } catch (Exception e) {
            LOGGER.error("Error has occured {}", e);
        }
    }

    // Fill template order parametres
    private Map<String, Object> buildParametrsMap(String pdfInvoiceParameters) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> pdfInvoiceParams = new HashMap<>();
        pdfInvoiceParams.put("paymentType", "przelew");
        pdfInvoiceParams.put("poNumber", "przelew");
        pdfInvoiceParams.put("customerEmail", "przelew");
        pdfInvoiceParams.put("customerName", "przelew");
        pdfInvoiceParams.put("invoiceNumber", "przelew");
        pdfInvoiceParams.put("invoiceDate", "przelew");
        pdfInvoiceParams.put("currency", "przelew");
        pdfInvoiceParams.put("productName", "przelew");
        pdfInvoiceParams.put("price", "przelew");
        pdfInvoiceParams.put("vat", "przelew");
        pdfInvoiceParams.put("lineTotal", "przelew");

//         pdfInvoiceParams.put("logo", resourcesInvoiceImagesPath + "logo.jpg");

        return pdfInvoiceParams;
    }

}

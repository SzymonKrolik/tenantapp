package com.mixez.tenantapartment.service;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Szymon Królik
 */
@Service
@AllArgsConstructor
public class JwtTokenService {
    private static String secret;
    private static int tokenExpirationTime;


    @Value("${jwt.secret}")
    public void setSecret(String secret) {
        JwtTokenService.secret = secret;
    }

    @Value("${jwt.jwtExpirationTime}")
    public void setTokenExpirationTime(int tokenExpirationTime) {
        JwtTokenService.tokenExpirationTime = tokenExpirationTime;
    }

    public static String generateJwtoken(String email, Claims claims) {
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setSubject(email)
                .setExpiration(Date.from(Instant.now().plusMillis(tokenExpirationTime)))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public static void verifyJwtoken(String token) throws JwtException {
        Jwts.parser()
                .setSigningKey(secret)
                .parse(token.substring(7));
    }

    public static Claims getClaimsFromJwtoken(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token.substring(7))
                .getBody();
    }

    public static String updateJwToken(String jwToken) {
        Claims claims = getClaimsFromJwtoken(jwToken);

        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setExpiration(Date.from(Instant.now().plusMillis(tokenExpirationTime)))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public static String getJwtokenFromHeader() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getHeader("Authorization");
    }
}
package com.mixez.tenantapartment.utils;

import lombok.experimental.UtilityClass;

import java.util.Base64;

@UtilityClass
public class CryptoUtils {

    public static String decodeBase64(String value) {
        byte[] decodedBytes = Base64.getDecoder().decode(value);
        return new String(decodedBytes);
    }

    public static String encodeBase64(String value) {
        return  Base64.getEncoder().encodeToString(value.getBytes());
    }
}

package com.mixez.tenantapartment.utils;


import com.mixez.tenantapartment.dto.DefaultPaginationRequest;
import lombok.experimental.UtilityClass;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Objects;

@UtilityClass
public class PageUtils {

    public static Pageable createPagination(DefaultPaginationRequest request) {
        Sort sort = createSort(request);

        return PageRequest.of(request.getPageNo(), Objects.equals(0, request.getPageSize()) ? 5 : request.getPageSize(), sort);
    }

    private Sort createSort(DefaultPaginationRequest request) {

        if (Objects.nonNull(request.getSortDir()) && Objects.nonNull(request.getSortBy())) {
            return request.getSortDir().equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(request.getSortBy()).ascending() : Sort.by(request.getSortBy()).descending();
        }

        return Sort.by("id").ascending();
    }
}

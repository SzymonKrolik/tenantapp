package com.mixez.tenantapartment.utils;



import io.netty.util.internal.StringUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.InvalidAlgorithmParameterException;

@UtilityClass
public class Utils {


    public static String getUserUuid() {
        String uuid = SecurityContextHolder.getContext().getAuthentication().getName();
        if (StringUtil.isNullOrEmpty(uuid))
            throw new BadCredentialsException("Bad credentials");
        return uuid;
    }
}

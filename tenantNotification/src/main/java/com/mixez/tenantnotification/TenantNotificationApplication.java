package com.mixez.tenantnotification;

import com.mixez.tenantnotification.dto.NotificationDTO;
import com.mixez.tenantnotification.enums.NotificationType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.kafka.core.KafkaTemplate;

@SpringBootApplication(exclude = {
        org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class}
)
@EnableCaching
public class TenantNotificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(TenantNotificationApplication.class, args);
    }



}

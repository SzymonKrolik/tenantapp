package com.mixez.tenantnotification.config;


import com.mixez.tenantnotification.dto.NotificationDTO;
import com.mixez.tenantnotification.enums.NotificationType;
import com.mixez.tenantnotification.service.EmailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class KafkaListeners {

    private final EmailService emailService;
    @KafkaListener(topics = "userNotification", groupId = "user", containerFactory = "kafkaListenerContainerFactory")
    void listener(@Payload NotificationDTO data) {
        switch (data.getNotificationType()) {
            case CONFIRM_REGISTRATION_EMAIL:
                emailService.sendRegistrationConfirmationMail(data);
        }
        log.info("LISTENER: {}", data);
    }
}

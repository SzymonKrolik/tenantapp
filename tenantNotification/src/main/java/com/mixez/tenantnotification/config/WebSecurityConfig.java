package com.mixez.tenantnotification.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

/**
 * @author Szymon Królik
 */
@Configuration

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig{

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserDetailsService userDetailsService;

    public WebSecurityConfig(BCryptPasswordEncoder bCryptPasswordEncoder, UserDetailsService userDetailsService) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userDetailsService = userDetailsService;
    }

//    @Bean
//    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
//        http.authorizeHttpRequests(authz -> authz
//                .requestMatchers(HttpMethod.POST,"/**").permitAll()
//                .requestMatchers(HttpMethod.GET,"/**").permitAll()
//                .requestMatchers(HttpMethod.PUT,"/**").permitAll()
//                .requestMatchers(HttpMethod.DELETE,"/**").permitAll()
//                .anyRequest().authenticated())
//                .csrf()
//                .disable();
//        http.requestMatchers()
//                .requestMatchers(HttpMethod.POST,"/**").permi
//
//
//        return http.build();
//    }



}


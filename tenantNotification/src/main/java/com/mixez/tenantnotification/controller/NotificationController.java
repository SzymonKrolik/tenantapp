package com.mixez.tenantnotification.controller;

import com.mixez.tenantnotification.dto.NotificationDTO;
import com.mixez.tenantnotification.service.NotificationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@Slf4j
@AllArgsConstructor
public class NotificationController {
    private final NotificationService notificationService;

    @PostMapping("/notify")
    public boolean sendNotification(@RequestBody NotificationDTO notificationDTO) {
        return notificationService.sendNotification(notificationDTO);
    }


}

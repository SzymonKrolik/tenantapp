package com.mixez.tenantnotification.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mixez.tenantnotification.enums.NotificationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NotificationDTO {
    @JsonProperty("notificationType")
    private NotificationType notificationType;
    @JsonProperty("recipientEmail")
    private String recipientEmail;
    @JsonProperty("name")
    private String name;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("link")
    private String link;

    @JsonProperty("content")
    private String content;
}

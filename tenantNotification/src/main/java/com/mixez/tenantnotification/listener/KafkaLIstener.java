package com.mixez.tenantnotification.listener;

import org.springframework.kafka.annotation.KafkaListener;

public class KafkaLIstener {
    @KafkaListener(topics = "notification_topic", groupId = "group-id")
    public void listen(String message) {
        System.out.println("Received Messasge in group - group-id: " + message);
    }
}

package com.mixez.tenantnotification.service;

import com.mixez.tenantnotification.dto.NotificationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@Slf4j
public class EmailService {
    @Autowired
    private JavaMailSenderImpl javaMailSender;
    public void sendRegistrationConfirmationMail(NotificationDTO data) {
        log.info("DATA: {}", data);
        javaMailSender.setDefaultEncoding("UTF-8");
        MimeMessage mail  = javaMailSender.createMimeMessage();
        try {
            mail.setContent("NIE KLIKEJ, TO JEST TEST: " + data.getLink(), "text/html; charset=ISO-8859-2");
            MimeMessageHelper helper = new MimeMessageHelper(mail, "UTF-8");
            helper.setTo(data.getRecipientEmail());
            helper.setSubject("KLIKNIJ");
            helper.setFrom("szefWszystkichSzefow@mail.pl");
            javaMailSender.send(mail);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}

package com.mixez.tenantnotification.service;

import com.mixez.tenantnotification.dto.NotificationDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class NotificationService {
    public boolean sendNotification(NotificationDTO notificationDTO) {
        log.info(":TGESTST");
        return true;
    }
}

package com.mixez.tenantuser;

import com.mixez.tenantuser.dto.NotificationDTO;
import com.mixez.tenantuser.enums.NotificationType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
public class TenantUserApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(TenantUserApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder application) {
        return application.sources (TenantUserApplication.class);
    }
//    @Bean
//    CommandLineRunner commandLineRunner(KafkaTemplate<String, NotificationDTO> kafkaTemplate) {
//        return args -> {
//            for (int i = 0; i < 10; i++) {
//                kafkaTemplate.send("userNotification", NotificationDTO.builder().notificationType(NotificationType.CONFIRM_REGISTRATION_EMAIL)
//                        .name("Jarek")
//                        .lastName("Kaczyński")
//                        .recipientEmail("mixezshop@gmail.com")
//                        .link("jakisFajnyLinki.pl")
//                        .build());
//            }
//
//        };
//    }
}

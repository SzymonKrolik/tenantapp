package com.mixez.tenantuser.client;


import com.mixez.tenantuser.dto.DictionaryDTO;
import com.mixez.tenantuser.dto.ParameterDTOResponse;
import com.mixez.tenantuser.enums.CommonUrlE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Klient dla common
 */
@Service
@Slf4j
public class CommonClient {
    private final WebClient commonWebClient;

    public CommonClient(@Value("${common.url}") String commonUrl) {
        this.commonWebClient = WebClient.builder()
                .baseUrl(commonUrl)
                .build();
    }
    //todo ogarnac wyjatki jaki rzuca ta usluga

    /**
     * Pobiera wartosci parametrów po kodzie
     * @param code kod parametru
     * @return wartosc parametru
     */
    public ParameterDTOResponse getParameterByCode(String code) {
        return commonWebClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(CommonUrlE.GET_PARAMETER_BY_VALUE.getUrl())
                        .queryParam("code", code)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(ParameterDTOResponse.class)
                .block();
    }

    /**
     * Pobiera wartosci słownika po kodzie
     * @param code Kod słownika
     * @return Wartosci słownika
     */
    public DictionaryDTO getDictionaryByCode(String code) {
        return commonWebClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(CommonUrlE.GET_DICTIONARY_BY_VALUE.getUrl())
                        .queryParam("code", code)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(DictionaryDTO.class)
                .block();
    }
}

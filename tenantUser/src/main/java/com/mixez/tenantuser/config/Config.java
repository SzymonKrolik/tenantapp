package com.mixez.tenantuser.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan({
    "com.tenants.common.dto",
        "com.tenants.common.enums"
}
)
public class Config {
}

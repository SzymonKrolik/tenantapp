package com.mixez.tenantuser.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@Profile("prod")
public class KafkaTopicConfig {
    @Bean
    public NewTopic userNotificationTopic() {
        return TopicBuilder.name("userNotification")
                .build();
    }
}

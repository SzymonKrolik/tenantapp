package com.mixez.tenantuser.controller;


import com.mixez.tenantuser.dto.ExceptionDetailsDTO;
import com.mixez.tenantuser.exception.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.ws.rs.core.Response;
import java.util.Date;

@ControllerAdvice
@RequiredArgsConstructor
public class ControllerAdvisor {

    @ExceptionHandler({LinkNotFoundException.class})
    public ResponseEntity<?> handleNotFoundException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({LinkExpiredException.class, UserPasswordExpiredException.class})
    public ResponseEntity<?> handleRequestTimeoutException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.REQUEST_TIMEOUT);
    }

    @ExceptionHandler({UserAlreadyActivateException.class})
    public ResponseEntity<?> handleConflictException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(exceptionDetailsDTO, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({InvalidRequestException.class})
    public ResponseEntity<?> handleBadRequestException(Exception exception, WebRequest request) {
        ExceptionDetailsDTO exceptionDetailsDTO = new ExceptionDetailsDTO(new Date(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(exceptionDetailsDTO, HttpStatus.BAD_REQUEST);
    }
}


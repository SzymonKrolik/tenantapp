package com.mixez.tenantuser.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthcheck")
@AllArgsConstructor
public class HealthCheckController {

    @GetMapping
    public String healthCheck() {
        return "User works";
    }
}

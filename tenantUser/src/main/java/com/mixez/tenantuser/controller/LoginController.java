package com.mixez.tenantuser.controller;

import com.mixez.tenantuser.dto.AccountInformationDTO;
import com.mixez.tenantuser.dto.login.LoginRequestDTO;
import com.mixez.tenantuser.dto.login.LoginResponseDTO;
import com.mixez.tenantuser.service.LoginService;
import com.mixez.tenantuser.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Kontoler odpowiedzialny za logowanie
 */
@RestController
@RequestMapping("/login")
@AllArgsConstructor
public class LoginController {

    private final LoginService loginService;

    private final UserService userService;

    /**
     * Loguje uzytkownika do konta
     * @param dto Dane logowania
     * @param request ServletRequest
     * @param response ServletResponse
     * @return Dane dot. logowania
     */
    @PostMapping
    public LoginResponseDTO loginUser(@RequestBody LoginRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
        return loginService.userLogin(dto, request, response);
    }

    /**
     * Loguje uzytkownika do danego konta firmy
     * @param dto dane logowania
     * @param request ServletRequest
     * @param response ServletResponse
     * @return Dane dot. logowania
     */
    @PostMapping("/login-to-account")
    public LoginResponseDTO loginToUserAccount(@RequestBody LoginRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
        return loginService.loginToUserAccount(dto, request, response);
    }
 }

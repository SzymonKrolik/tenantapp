package com.mixez.tenantuser.controller;


import com.mixez.tenantuser.dto.register.UserRegisterRequest;
import com.mixez.tenantuser.service.RegisterService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Rejestracja kontroller
 */
@RestController
@RequestMapping("/register")
@AllArgsConstructor
public class RegisterController {
    private final RegisterService registerService;

    @PostMapping
    public boolean registerUserAccount(HttpServletRequest request, HttpServletResponse response, @Valid @RequestBody UserRegisterRequest dto) {
        return registerService.registerUser(dto);
    }

    @PostMapping("/validate-token")
    public boolean validateUserToken(HttpServletRequest request, HttpServletResponse response, @RequestBody String token) {
        return registerService.validateUserToken(token);
    }

//    @PostMapping("/create-account")
//    public boolean createAccount(HttpServletRequest request, HttpServletResponse response, @Valid @RequestBody CreateCompanyRequestDTO dto) {
//        return false;
//    }
}

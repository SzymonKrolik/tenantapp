package com.mixez.tenantuser.controller;

import com.mixez.tenantuser.dto.AccountInformationDTO;
import com.mixez.tenantuser.enums.AccountTypeE;
import com.mixez.tenantuser.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/accounts")
    public List<AccountInformationDTO> getAccounts(HttpServletRequest request){
        return userService.getUserAccountList(request);
    }
}

package com.mixez.tenantuser.controller.api;


import com.mixez.tenantuser.dto.UserInformationForCompanyRegistrationDTO;
import com.mixez.tenantuser.dto.UserInformationWithIdDTO;
import com.mixez.tenantuser.dto.UserUuidRequest;
import com.mixez.tenantuser.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Kontroler uzytkownika dla usług wewnętrznych
 */
@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
@Slf4j
public class UserApiController {
    private final UserService userService;

    /**
     * Pobiera informacje o użytkowniku po tokenie
     * @param request ServletRequest
     * @return Informacje o uzytkownku
     */
    @PostMapping("/by-token")
    public UserInformationWithIdDTO getUserInformationBnyToken(HttpServletRequest request) {
        return null;
    }

    /**
     * Pobiera informacje o uzytkowniku podczas rejestrowania firmy/mieszkania
     * @param request UUID uzytkownika
     * @return Informacje uzytkownika
     */
    @PostMapping
    public UserInformationForCompanyRegistrationDTO getUserInformationForCompanyRegistration(@RequestBody UserUuidRequest request) {
        return userService.getUserInformationForCompanyRegistration(request);
    }

}

package com.mixez.tenantuser.dto;

import com.mixez.tenantuser.enums.AccountTypeE;
import lombok.Builder;
import lombok.Data;

@Data
public class AccountInformationDTO {
    private String name;
    private AccountTypeE accountType;
    private String email;
    private String uuid;
}

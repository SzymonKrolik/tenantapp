package com.mixez.tenantuser.dto;

import lombok.Data;

@Data
public class AddressDTO {
    private String city;
    private String street;
    private String number;
}

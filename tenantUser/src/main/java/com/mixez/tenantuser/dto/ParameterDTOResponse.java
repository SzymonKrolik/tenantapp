package com.mixez.tenantuser.dto;

import lombok.Data;

@Data
public class ParameterDTOResponse {
    private String code;
    private String value;
}

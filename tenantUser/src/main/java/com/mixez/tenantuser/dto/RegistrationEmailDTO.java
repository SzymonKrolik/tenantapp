package com.mixez.tenantuser.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegistrationEmailDTO {
    private String receiver;
    private String message;
}

package com.mixez.tenantuser.dto;

import lombok.Data;

@Data
public class UserInformationForCompanyRegistrationDTO {
    private String accountType;
    private String uuid;
    private String email;
    private int accountCount;
}

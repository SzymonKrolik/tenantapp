package com.mixez.tenantuser.dto;

import lombok.Data;

@Data
public class UserInformationWithIdDTO {
    private String email;
    private String uuid;
    private Long id;
}

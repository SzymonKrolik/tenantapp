package com.mixez.tenantuser.dto;

import lombok.Data;

@Data
public class UserUuidRequest {
    private String uuid;
}

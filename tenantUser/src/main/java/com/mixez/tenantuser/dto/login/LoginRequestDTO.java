package com.mixez.tenantuser.dto.login;

import lombok.Data;

@Data

public class LoginRequestDTO {
    private String email;
    private String password;

    private String uuid;
}

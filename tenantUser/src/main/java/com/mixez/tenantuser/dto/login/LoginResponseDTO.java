package com.mixez.tenantuser.dto.login;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginResponseDTO {
    private boolean logged;
    private boolean passwordExpired;
    private boolean accountLocked;
    private String message;
    private boolean enabled;
    private String token;
}

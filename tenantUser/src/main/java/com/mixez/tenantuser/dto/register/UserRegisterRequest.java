package com.mixez.tenantuser.dto.register;

import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Data
public class UserRegisterRequest {
    @Size(min = 1)
    private String email;
    @Size(min = 1)
    private String phoneNumber;
    @Size(min = 1)
    private String name;
    @Size(min = 1)
    private String lastName;
    @Size(min = 1)
    private String password;
    @Size(min = 1)
    private String matchingPassword;
    @Size(min = 1)
    private LocalDate birthDate;
}

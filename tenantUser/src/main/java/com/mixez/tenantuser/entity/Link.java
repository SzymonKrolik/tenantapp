package com.mixez.tenantuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mixez.tenantuser.enums.LinkStatusE;
import com.mixez.tenantuser.enums.LinkTypeE;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;

import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER_LINK")
public class Link {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LINK_ID")
    private Long linkId;

    @Column(name = "LINK_VALUE")
    private String link;

    @Column(name = "LINK_TYPE")
    @Enumerated(EnumType.STRING)
    private LinkTypeE linkTypeE;


    @Column(name = "LINK_STATUS")
    @Enumerated(EnumType.STRING)
    private LinkStatusE linkStatusE;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    @JsonIgnore
    private User user;

    @Column(name = "RECEIVER")
    private String receiver;

    @CreatedBy
    private String createdBy;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;
}

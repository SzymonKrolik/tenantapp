package com.mixez.tenantuser.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

/**
 * @author Szymon Królik
 */
@Data
@Entity
@Table(name = "REFRESH_TOKEN")
public class RefreshToken {
    @Id
    @Column(name = "REFRESH_TOKEN_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, nullable = false, name = "TOKEN")
    private String token;

    @Column(nullable = false, name = "EXPIRATION_TIME")
    private Instant expirationDate;

    @OneToOne
    @JoinColumn(name = "USER", referencedColumnName = "USER_ID")
    private User user;
}

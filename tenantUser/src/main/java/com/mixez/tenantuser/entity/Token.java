package com.mixez.tenantuser.entity;


import com.mixez.tenantuser.enums.TokenStatusE;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "USER_TOKEN")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TOKEN_ID")
    private Long tokenId;

    @Column(name = "TOKEN")
    private String token;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "TOKEN_STATUS")
    @Enumerated(EnumType.STRING)
    private TokenStatusE tokenStatusE;
    @Column(name = "CREATED_AT")
    @CreationTimestamp
    private LocalDateTime createdAt;

}

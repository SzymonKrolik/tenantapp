package com.mixez.tenantuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mixez.tenantuser.enums.AccountTypeE;
import javax.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "USERS")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Long id;
    @Column(nullable = false, name = "NAME")
    private String name;
    @Column(unique = true, nullable = false, name = "EMAIL")
    private String email;
    @Column(nullable = false, name = "PASSWORD")
    private String password;
    @Column(unique = true, nullable = false, name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(unique = false, nullable = false, name = "LAST_NAME")
    private String lastName;

    @Column(nullable = false, name = "LOCKED")
    private boolean locked = false;

    @Column(nullable = false, name = "ENABLED")
    private boolean enabled = false;

    @Column(nullable = false, name = "BIRTHDATE")
    private LocalDate birthDate;
    @Column(name = "ACCOUNT_TYPE")
    @Enumerated(EnumType.STRING)
    private AccountTypeE accountType;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "IDENTITY_NUMBER")
    private String identityNumber;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<UserRole> roles;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<Link> userLinks = new HashSet<>();

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<ConfirmationToken> userToken = new HashSet<>();
    @CreatedBy
    private String createdBy;
    @CreationTimestamp
    @Column(updatable = false)
    private Date createdAt;

    @UpdateTimestamp
    private Date updatedAt;

    @Column(name = "PASSWORD_EXPIRATION_DATE")
    private LocalDateTime passwordExpirationDate;

    @EqualsAndHashCode.Exclude
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ADDRESS_ID", referencedColumnName = "ADDRESS_ID")
    private Address address;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getRoleName()))
                .collect(Collectors.toSet());
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }
}

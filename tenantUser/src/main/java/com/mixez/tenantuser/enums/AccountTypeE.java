package com.mixez.tenantuser.enums;

import lombok.Getter;

public enum AccountTypeE {
    ADM("ADMIN"),
    ORG("FIRMA"),
    TEN("NAJEMNCA"),
    USR("UZYTKOWNIK"),
    UNAUTH_USR("UNAUTH_USER");


    @Getter
    private final String accountType;
    AccountTypeE(String accountType) {
        this.accountType = accountType;
    }

    public static AccountTypeE getValue(String x) {
        AccountTypeE accountTypeE = null;
        for (AccountTypeE type : AccountTypeE.values()) {
            if (type.getAccountType().equals(x))
                accountTypeE = type;
        }
        return accountTypeE;
    }
}

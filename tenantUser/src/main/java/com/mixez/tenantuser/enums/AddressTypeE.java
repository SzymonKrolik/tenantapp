package com.mixez.tenantuser.enums;

import lombok.Getter;

public enum AddressTypeE {
    KORES("ADRES_KORESPONDENCYJNY"),
    GLOW("ADRES_ZAMELDOWANIA");

    @Getter
    private final String addressType;
    AddressTypeE(String addressType) {
        this.addressType = addressType;
    }

    public static AddressTypeE getValue(String x) {
        AddressTypeE addressType = null;
        for (AddressTypeE type : AddressTypeE.values()) {
            if (type.getAddressType().equals(x))
                addressType = type;
        }
        return addressType;
    }
}

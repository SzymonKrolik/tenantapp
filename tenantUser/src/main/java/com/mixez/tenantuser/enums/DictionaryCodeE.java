package com.mixez.tenantuser.enums;

import lombok.Getter;

public enum DictionaryCodeE {
    ROLES_FOR_REGISTER("REGISTER_ROLES"),
    ROLES_FORM_NORMAL_USER("USER_ROLES");

    @Getter
    private final String dictionaryCode;
    DictionaryCodeE(String description) {
        this.dictionaryCode = description;
    }

    public static DictionaryCodeE getValue(String x) {
        DictionaryCodeE dictionaryCode = null;
        for (DictionaryCodeE type : DictionaryCodeE.values()) {
            if (type.getDictionaryCode().equals(x))
                dictionaryCode = type;
        }
        return dictionaryCode;
    }
}

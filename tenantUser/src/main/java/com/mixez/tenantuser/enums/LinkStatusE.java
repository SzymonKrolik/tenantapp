package com.mixez.tenantuser.enums;

public enum LinkStatusE {
    ACTIVE,
    EXPIRED,
    USED
}

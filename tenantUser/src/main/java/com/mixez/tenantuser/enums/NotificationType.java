package com.mixez.tenantuser.enums;


import lombok.Data;

public enum NotificationType {
    CONFIRM_REGISTRATION_EMAIL;
}

package com.mixez.tenantuser.enums;

public enum TokenStatusE {
    ACTIVE,
    USED,
    EXPIRED;
}

package com.mixez.tenantuser.exception;

public class ApiErrorException extends RuntimeException {
    private static final String MSG = "Api error";

    public ApiErrorException() {
        super(MSG);
    }
}

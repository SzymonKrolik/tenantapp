package com.mixez.tenantuser.exception;

public class LinkExpiredException extends RuntimeException {
    private static final String MSG = "Link expired exception";

    public LinkExpiredException() {
        super(MSG);
    }
}

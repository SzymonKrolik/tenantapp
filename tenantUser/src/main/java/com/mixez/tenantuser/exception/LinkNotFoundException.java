package com.mixez.tenantuser.exception;

public class LinkNotFoundException extends RuntimeException {
    private static final String MSG = "Link not found exception";

    public LinkNotFoundException() {
        super(MSG);
    }
}

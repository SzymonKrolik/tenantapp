package com.mixez.tenantuser.exception;

public class RefreshTokenExpiredException extends RuntimeException {
    private static final String MSG = "Refresh token expired";

    public RefreshTokenExpiredException() {
        super(MSG);
    }
}

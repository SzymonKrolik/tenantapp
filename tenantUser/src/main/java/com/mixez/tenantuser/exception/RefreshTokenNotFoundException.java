package com.mixez.tenantuser.exception;

public class RefreshTokenNotFoundException extends RuntimeException {
    private static final String MSG = "Refresh token not found";

    public RefreshTokenNotFoundException() {
        super(MSG);
    }
}

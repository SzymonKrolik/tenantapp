package com.mixez.tenantuser.exception;

public class UserAlreadyActivateException extends RuntimeException {
    private static final String MSG = "User already activate exception";

    public UserAlreadyActivateException() {
        super(MSG);
    }
}

package com.mixez.tenantuser.exception;

public class UserNotFoundException extends RuntimeException {
    private static final String MSG = "User not found";

    public UserNotFoundException() {
        super(MSG);
    }
}

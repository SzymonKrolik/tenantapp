package com.mixez.tenantuser.exception;

public class UserPasswordExpiredException extends RuntimeException {
    private static final String MSG = "Password expired, reset link was send";

    public UserPasswordExpiredException() {
        super(MSG);
    }
}

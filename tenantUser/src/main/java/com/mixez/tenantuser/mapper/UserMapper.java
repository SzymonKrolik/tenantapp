package com.mixez.tenantuser.mapper;

import com.mixez.tenantuser.dto.AccountInformationDTO;
import com.mixez.tenantuser.dto.UserInformationForCompanyRegistrationDTO;
import com.mixez.tenantuser.dto.UserInformationWithIdDTO;
import com.mixez.tenantuser.dto.register.UserRegisterRequest;
import com.mixez.tenantuser.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toEntity(UserRegisterRequest request);

    @Mapping(target = "uuid", source = "uuid")
    AccountInformationDTO toAccountInformationDTO(User user);

    @Mapping(target = "uuid", source = "uuid")
    UserInformationForCompanyRegistrationDTO toUserCompanyRegistration(User user);
}

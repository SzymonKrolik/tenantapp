package com.mixez.tenantuser.repository;

import com.mixez.tenantuser.entity.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


public interface LinkRepository extends JpaRepository<Link, Long> {

    @Query("SELECT l FROM Link l WHERE l.link = :link")
    Optional<Link> findLinkByLink(@Param("link") String link);

}

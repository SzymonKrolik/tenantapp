package com.mixez.tenantuser.repository;

import com.mixez.tenantuser.entity.Token;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TokenRepository extends JpaRepository<Token, Long> {

}

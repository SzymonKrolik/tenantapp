package com.mixez.tenantuser.repository;

import com.mixez.tenantuser.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsUserByEmail(String email);
    Optional<User> findByEmail(String email);

    @Query("SELECT u FROM User u WHERE u.uuid = :uuid")
    List<User> findUsersAccountByUuid(@Param("uuid") String uuid);

    Optional<User> findByUuid(String uuid);

}

package com.mixez.tenantuser.repository;


import com.mixez.tenantuser.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Set;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

   Set<UserRole> findByRoleNameIn(Set<String> roleNames);

}

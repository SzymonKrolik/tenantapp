package com.mixez.tenantuser.service;

import com.mixez.tenantuser.client.CommonClient;
import com.mixez.tenantuser.entity.Link;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.enums.LinkStatusE;
import com.mixez.tenantuser.enums.LinkTypeE;
import com.mixez.tenantuser.enums.ParameterCodeE;
import com.mixez.tenantuser.exception.LinkExpiredException;
import com.mixez.tenantuser.exception.LinkNotFoundException;
import com.mixez.tenantuser.repository.LinkRepository;
import com.mixez.tenantuser.utils.CryptoUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Service
@Slf4j
@AllArgsConstructor
public class LinkService {
    private final LinkRepository linkRepository;
    private final CommonClient commonClient;

    public String generateLink(User user, LinkTypeE linkTypeE) {
        StringBuilder builder = new StringBuilder();

        switch (linkTypeE) {
            case ACTIVATION:
                builder.append("http://localhost:4200/registration/confirm/");
                String userToken = buildUserToken(builder, user, linkTypeE);
                builder.append(userToken);
                linkRepository.save(Link.builder().link(userToken).linkTypeE(linkTypeE)
                                .createdAt(LocalDateTime.now())
                                .linkStatusE(LinkStatusE.ACTIVE)
                                .user(user)
                        .build());
                return builder.toString();
        }
        return "";
    }

    private String buildUserToken(StringBuilder builder, User user, LinkTypeE linkTypeE) {
        String userToken =  user.getName() +  ";" + user.getEmail() +  ";" + linkTypeE.name() + ";" + LocalDateTime.now().toString();
        return CryptoUtils.encodeBase64(userToken);
    }

    public Link getLinkByLink(String link) {
        return linkRepository.findLinkByLink(link).orElseThrow(LinkNotFoundException::new);
    }

    public void validateLink(Link link) {
        if (link.getLinkStatusE().equals(LinkStatusE.EXPIRED))
            throw new LinkExpiredException();
        LocalDateTime expirationTime = link.getCreatedAt();

        int expirationLinkParameter = Integer.parseInt(commonClient.getParameterByCode(ParameterCodeE.CZAS_WAZN_LINK_AKT.toString()).getValue());

        LocalDateTime expirationLinkType = LocalDateTime.now().plusMinutes(Long.valueOf(expirationLinkParameter));
        if (expirationTime.isAfter(expirationLinkType))
            throw new LinkExpiredException();

    }

    public void saveLink(Link userLink) {
        linkRepository.save(userLink);
    }
}

package com.mixez.tenantuser.service;

import com.mixez.tenantuser.dto.login.LoginRequestDTO;
import com.mixez.tenantuser.dto.login.LoginResponseDTO;
import com.mixez.tenantuser.entity.RefreshToken;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.exception.UserNotFoundException;
import com.mixez.tenantuser.repository.UserRepository;
import com.mixez.tenantuser.utils.CookieUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.security.auth.Login;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class LoginService {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RefreshTokenService refreshTokenService;

    public LoginResponseDTO userLogin(LoginRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {
        if (Objects.isNull(dto.getEmail()) || Objects.isNull(dto.getPassword()))
            return LoginResponseDTO.builder().logged(false).build();

        if (!userService.userExistByEmail(dto.getEmail()))
            return LoginResponseDTO.builder().logged(false).build();

        User user = userService.getUserByEmail(dto.getEmail());

        if (!user.isEnabled())
            return LoginResponseDTO.builder().enabled(false).logged(false).build();
        if (user.isLocked())
            return LoginResponseDTO.builder().accountLocked(true).logged(false).build();

        if (!bCryptPasswordEncoder.matches(dto.getPassword(), user.getPassword()))
            return LoginResponseDTO.builder().logged(false).build();

        //Create refresh token
        refreshTokenService.createRefreshToken(user.getId());


        //Create JWToken
        Claims claims = new DefaultClaims();
        Set<String> authorities = user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
        claims.put("authorities", authorities);
        String jwtToken = JwtTokenService.generateJwtoken(user.getUuid(), claims);
        return LoginResponseDTO.builder().logged(true)
                .token(jwtToken)
                .accountLocked(false)
                .enabled(true)
                .build();
    }

    public LoginResponseDTO loginToUserAccount(LoginRequestDTO dto, HttpServletRequest request, HttpServletResponse response) {

        List<User> userList = userService.getAllUsersAccount(request);

        User user = userList.stream()
                .filter(account -> account.getUuid().equalsIgnoreCase(dto.getUuid()))
                .findAny()
                .orElseThrow(UserNotFoundException::new);

        userService.validateUserAccount(user);

        //Create refresh token
        refreshTokenService.createRefreshToken(user.getId());


        //Create JWToken
        Claims claims = new DefaultClaims();
        Set<String> authorities = user.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
        claims.put("authorities", authorities);
        String jwtToken = JwtTokenService.generateJwtoken(user.getUuid(), claims);
        return LoginResponseDTO.builder().logged(true)
                .token(jwtToken)
                .accountLocked(false)
                .enabled(true)
                .build();
    }
}

package com.mixez.tenantuser.service;

import com.mixez.tenantuser.dto.NotificationDTO;
import com.mixez.tenantuser.enums.NotificationType;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class NotificationService {

    private final KafkaTemplate<String, NotificationDTO> kafkaTemplate;
    private static String REGISTRATION_MAIL = "<html>" +
            "<head>" +
            "<meta charset='UTF-8'>" +
            "</head>" +
            "<body> " +
            "<h2> Hej %s %s!</h2>" +
            "<h2> Dziękujemy za rejestrację, proszę kliknąć w link aby aktywować konto </h2>" +
//                                                "<a href='http://146.59.3.226:8082/event-api/user/confirm?token=%s'>Aktywuj konto</a>" +
            "<a href='%s'>Aktywuj konto</a>" +
            "<hr>"+
            "Z pozdrowieniami, <br> " +
            "Zespół Whatssup!<br>" +
            "Kontakt: eventmailingv1@gmail.com" +
            "</body>" +
            "</html>";

    public void sendUserNotification(NotificationDTO dto) {
        kafkaTemplate.send("userNotification", dto);

    }

    public void sendNotificationToUser(String email, String lastName, String name, String link, NotificationType notificationType) {
        NotificationDTO request = NotificationDTO.builder()
                .notificationType(notificationType)
                .recipientEmail(email)
                .name(name)
                .lastName(lastName)
                .content(createMailMessageNewAccount(name, lastName, link))
                .build();

        kafkaTemplate.send("userNotification", request);
    }

    private String createMailMessageNewAccount(String name, String lastName, String link) {
        return String.format(REGISTRATION_MAIL, name, lastName, link);
    }

}

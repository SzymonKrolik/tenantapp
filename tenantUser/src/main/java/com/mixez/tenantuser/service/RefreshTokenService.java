package com.mixez.tenantuser.service;

import com.mixez.tenantuser.entity.RefreshToken;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.exception.RefreshTokenNotFoundException;
import com.mixez.tenantuser.repository.RefreshTokenRepository;
import com.mixez.tenantuser.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Szymon Królik
 */
@Service
@AllArgsConstructor
@Slf4j
public class RefreshTokenService {
    private final RefreshTokenRepository refreshTokenRepository;
    private final UserService userService;
    private static int refreshTokenExpirationTime;

    @Value("${jwt.refreshTokenExpirationTime}")
    public void setRefreshTokenExpirationTime(int refreshTokenExpirationTime) {
        RefreshTokenService.refreshTokenExpirationTime = refreshTokenExpirationTime;
    }

    public String createRefreshToken(Long userId) {

        User user = userService.getUserById(userId);
        String token = UUID.randomUUID().toString();
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setUser(user);
        refreshToken.setExpirationDate(Instant.now().plusMillis(refreshTokenExpirationTime));
        refreshToken.setToken(token);
        refreshTokenRepository.save(refreshToken);

        return token;
    }

    public boolean verifyRefreshTokenExpiration(RefreshToken refreshToken) {
        if (refreshToken.getExpirationDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(refreshToken);
            return false;
        }
        return true;
    }

    public void deleteRefreshToken(Long userId) {
        RefreshToken refreshToken = findRefreshTokenByUserId(userId);
        try {
            refreshTokenRepository.delete(refreshToken);
        } catch (Exception ex) {
            log.info("Delete refresh token exception: {}", ex.getMessage());
    }
    }

    private RefreshToken findRefreshTokenByUserId(Long userId) {
        return refreshTokenRepository.findByUserId(userId)
                .orElseThrow(RefreshTokenNotFoundException::new);
    }

    public RefreshToken getByToken(String token) {
        return refreshTokenRepository.findByToken(token).orElseThrow(RefreshTokenNotFoundException::new);
    }


}

package com.mixez.tenantuser.service;

import com.mixez.tenantuser.dto.NotificationDTO;
import com.mixez.tenantuser.dto.register.UserRegisterRequest;
import com.mixez.tenantuser.entity.Link;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.enums.*;
import com.mixez.tenantuser.exception.ApiErrorException;
import com.mixez.tenantuser.exception.InvalidRequestException;
import com.mixez.tenantuser.exception.UserAlreadyExistException;
import com.mixez.tenantuser.mapper.UserMapper;
import com.mixez.tenantuser.repository.UserRepository;
import com.mixez.tenantuser.utils.CryptoUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
@Slf4j
public class RegisterService {
    //todo potwaierdzenie rejestracji dorobić
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserMapper userMapper;
    private final RoleService roleService;
    private final LinkService lInkService;

    private final UserService userService;
    private final NotificationService notificationService;
    private final String PHONE_NUMBER_REGEX = "^\\d{9}$";
    private final String EMAIL_REGEX = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}";
    private final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()\\\\[\\\\]{}:;',?/*~$^+=<>]).{8,20}$\n";

    public boolean registerUser(UserRegisterRequest dto) {
        checkIfCreateRequestIsValid(dto);
        if (checkIfUserExistByEmail(dto.getEmail()))
            throw new UserAlreadyExistException();
        validedPassword(dto);
        dto.setPassword(encodePassword(CryptoUtils.decodeBase64(dto.getPassword())));
        try {
            User user = userMapper.toEntity(dto);
            user.setUuid(UUID.randomUUID().toString());
            user.setAccountType(AccountTypeE.UNAUTH_USR);
            user.setPasswordExpirationDate(LocalDateTime.now().plusMonths(1L));
            User savedUser = userRepository.save(user);

            roleService.saveUserRole(savedUser, DictionaryCodeE.ROLES_FOR_REGISTER);
            userRepository.save(savedUser);
            String registrationLink = lInkService.generateLink(savedUser, LinkTypeE.ACTIVATION);
//            notificationService.sendNotificationToUser(user.getEmail(), user.getName(), user.getLastName(), registrationLink, NotificationType.CONFIRM_REGISTRATION_EMAIL);
        } catch (Exception ex) {
            log.info("USER_REGISTER_ERROR: {}, {}", dto.getEmail(), ex.getMessage());
            throw new ApiErrorException();
        }
        return true;
    }

    private void validedPassword(UserRegisterRequest dto) {
        //todo wiecej walidacji hasła (znaki specjalne itp)
        String password = CryptoUtils.decodeBase64(dto.getPassword());
        String matchingPassword = CryptoUtils.decodeBase64(dto.getMatchingPassword());
        Pattern passwordRegex = Pattern.compile(PASSWORD_REGEX);

        if (!password.equalsIgnoreCase(matchingPassword))
            throw new InvalidRequestException("Passwords dont match");

        if (!passwordRegex.matcher(dto.getPassword()).matches())
            throw new InvalidRequestException("Password is too simple");
    }

    private void checkIfCreateRequestIsValid(UserRegisterRequest dto) {
        Pattern phoneNumberPattern = Pattern.compile(PHONE_NUMBER_REGEX);
        Pattern emailPattern = Pattern.compile(EMAIL_REGEX);

        if (Objects.isNull(dto))
            throw new InvalidRequestException("Object cannot be null");

        if (!phoneNumberPattern.matcher(dto.getPhoneNumber()).matches())
            throw new InvalidRequestException("Invalid phone number format");
        if (!emailPattern.matcher(dto.getEmail()).matches())
            throw new InvalidRequestException("Invalid email format");

        if (Objects.isNull(dto.getBirthDate()))
            throw new InvalidRequestException("Invalid birthdate");

        LocalDate adultPersonBirthdate = LocalDate.now().minusYears(18L);

        if (dto.getBirthDate().isAfter(adultPersonBirthdate))
            throw new InvalidRequestException("You should be adult person");
    }

    private boolean checkIfUserExistByEmail(String email) {
        return userRepository.existsUserByEmail(email);
    }

    private String encodePassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }

    public boolean validateUserToken(String token) {
        Link userLink = lInkService.getLinkByLink(token);
        User user = userLink.getUser();
        lInkService.validateLink(userLink);
        userService.checkIfUserAlreadyActiveOrBlock(userLink.getUser());

        userService.activateUser(user);
        roleService.saveUserRole(user, DictionaryCodeE.ROLES_FORM_NORMAL_USER);
        userRepository.save(user);
        userLink.setLinkStatusE(LinkStatusE.EXPIRED);
        lInkService.saveLink(userLink);
        return true;
    }
}

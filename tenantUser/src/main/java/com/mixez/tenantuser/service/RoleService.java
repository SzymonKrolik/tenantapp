package com.mixez.tenantuser.service;

import com.mixez.tenantuser.client.CommonClient;
import com.mixez.tenantuser.dto.DictionaryDTO;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.entity.UserRole;
import com.mixez.tenantuser.enums.DictionaryCodeE;
import com.mixez.tenantuser.repository.UserRoleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class RoleService {
    //todo rtoles
    private final UserRoleRepository userRoleRepository;
    private final CommonClient commonClient;

    public void saveUserRole(User savedUser, DictionaryCodeE rolesForRegister) {

        DictionaryDTO rolesForUserDict = commonClient.getDictionaryByCode(rolesForRegister.getDictionaryCode());
        Set<UserRole> userRoles = userRoleRepository.findByRoleNameIn(rolesForUserDict.getValues());


        savedUser.setRoles(userRoles);
        log.info("USER_ROLES: {}", userRoles);
    }

}


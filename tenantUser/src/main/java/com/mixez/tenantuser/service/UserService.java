package com.mixez.tenantuser.service;


import com.mixez.tenantuser.dto.AccountInformationDTO;
import com.mixez.tenantuser.dto.UserInformationForCompanyRegistrationDTO;
import com.mixez.tenantuser.dto.UserInformationWithIdDTO;
import com.mixez.tenantuser.dto.UserUuidRequest;
import com.mixez.tenantuser.entity.User;
import com.mixez.tenantuser.enums.AccountTypeE;
import com.mixez.tenantuser.exception.*;
import com.mixez.tenantuser.mapper.UserMapper;
import com.mixez.tenantuser.repository.UserRepository;
import com.mixez.tenantuser.utils.CookieUtils;
import com.mixez.tenantuser.utils.Utils;
import io.netty.util.internal.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.NotFound;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    public boolean userExistByEmail(String email) {
        return userRepository.existsUserByEmail(email);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    private String getUserEmailFromContext() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        if (StringUtil.isNullOrEmpty(email))
            throw new RefreshTokenNotFoundException();
        return email;
    }

    public void checkIfUserAlreadyActiveOrBlock(User user) {
        if (user.isEnabled() || user.isLocked())
            throw new UserAlreadyActivateException();
    }

    public void activateUser(User user) {
        user.setEnabled(Boolean.TRUE);
        user.setLocked(Boolean.FALSE);
        user.setAccountType(AccountTypeE.USR);
    }

    public User getUserByUuid(String uuid) {
        return userRepository.findByUuid(uuid).orElseThrow(UserNotFoundException::new);
    }

    private List<User> getUserAccountsByUuid(String uuid) {
        return userRepository.findUsersAccountByUuid(uuid);
    }

    public List<AccountInformationDTO> getUserAccountList(HttpServletRequest request) {

        User user = getUserByUuid(Utils.getUserUuid());

        List<User> accountList = getUserAccountsByUuid(user.getUuid());

        return accountList.stream().map(account -> userMapper.toAccountInformationDTO(account))
                .collect(Collectors.toList());
    }

    public List<User> getAllUsersAccount(HttpServletRequest request) {
        return getUserAccountsByUuid(Utils.getUserUuid());
    }


    public void validateUserAccount(User user) {
        if (!user.isEnabled() || user.isLocked())
            throw new UserAlreadyActivateException();

        //todo wysyłka maila z resetem hasła
        if (user.getPasswordExpirationDate().isBefore(LocalDateTime.now()))
            throw new UserPasswordExpiredException();
    }




    public UserInformationForCompanyRegistrationDTO getUserInformationForCompanyRegistration(UserUuidRequest request) {
        User actualUser = userRepository.findByUuid(request.getUuid()).orElseThrow(UserNotFoundException::new);

        List<User> userAccounts = userRepository.findUsersAccountByUuid(actualUser.getUuid());

        UserInformationForCompanyRegistrationDTO response = userMapper.toUserCompanyRegistration(actualUser);
        //-1 bo nie liczymy normalnego konta
        response.setAccountCount(userAccounts.size() - 1);
        response.setAccountType(actualUser.getAccountType().getAccountType());

        return response;
    }
}

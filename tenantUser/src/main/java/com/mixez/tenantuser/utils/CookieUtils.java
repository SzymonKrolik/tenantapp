package com.mixez.tenantuser.utils;

import lombok.experimental.UtilityClass;
import org.apache.kafka.common.protocol.types.Field;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@UtilityClass
public class CookieUtils {

    private static final String TOKEN_COOKIE = "TOKEN";

    public static Cookie createCookie(String cookieName, String cookieValue) {
        Cookie cookie = new Cookie(cookieName, cookieValue);

        cookie.setMaxAge(86400);
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
//        cookie.setPath("/");
//        cookie.setDomain("example.com");

        return cookie;
    }

    public static String getTokenFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equalsIgnoreCase(TOKEN_COOKIE)) {
                return cookies[i].getValue();
            }
        }

        return null;
    }
}

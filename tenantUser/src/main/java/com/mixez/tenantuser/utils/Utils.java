package com.mixez.tenantuser.utils;


import com.mixez.tenantuser.exception.CookieNotFoundException;
import io.netty.util.internal.StringUtil;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class Utils {


    public static String getUserUuid() {
        String uuid = SecurityContextHolder.getContext().getAuthentication().getName();
        if (StringUtil.isNullOrEmpty(uuid))
            throw new CookieNotFoundException();
        return uuid;
    }
}
